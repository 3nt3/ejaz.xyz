import React, { Fragment } from "react";

import {
  HashRouter,
  Switch,
  Route
} from "react-router-dom";

import Home from "./Home.jsx";

function App() {
  return (
    <React.Fragment>
      <HashRouter>
        <Switch>
          <Route path="/" exact component={Home}/>
        </Switch>
      </HashRouter>
    </React.Fragment>
  );
}

export default App;

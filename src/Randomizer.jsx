import React from "react";

const CHOICES = [
  "little jakob",
  "papa ari",
  "aybizzle",
  "sanbizzle",
  "greta",
  "johanna",
  "johnie",
  "niels",
  "wassim saighani"
];

const Choosen = props => {
  return (
    <div>
      <h1>{props.name}</h1>
      <div>
        <button>{props.name} von nun an ignorieren</button>
        <button>weiter</button>
      </div>
    </div>
  );
};

class Randomizer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ignored: [],
      choices: CHOICES,
      choosen: undefined
    };
  }

  ignore(choosenID) {
    // add to ignored array
    this.setState({
      ignored: [...this.state.ignored, this.state.choices[choosenID]]
    });

    // delete from choices
    let choices = [];

    for (let c of this.state.choices) {
      if (c === this.state.choices[choosenID]) {
        continue;
      }

      choices.push(c);
    }

    this.setState({
      choices
    });
  }

  generate() {
    // determine candidate
    const choosenID = Math.floor(Math.random() * this.state.choices.length);

    if (this.state.choices.length > 0) {
      this.setState({
        choosen: this.state.choices[choosenID]
      });
    }

    return choosenID;
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    if (nextState.choices.length <= 0) {
      return { choices: CHOICES, ignored: [] };
    }
  }

  render() {
    return (
      <div>
        <h1>ZUFALLSGENERATOR</h1>
        <p>
          ein zufallsgenerator, welcher eine*n der schüler*innen der klasse 9c
          zufällig auswählt
        </p>

        <Choosen name={this.state.choosen} />

        <button
          onClick={() => {
            this.ignore(this.generate());
          }}
        >
          LOS
        </button>

        <div>
          <p>ignorierte</p>
          {this.state.ignored.map(item => (
            <span key={item}>{item}, </span>
          ))}
        </div>
      </div>
    );
  }
}

export default Randomizer;

import React from "react";
import Randomizer from "./Randomizer";

class Home extends React.Component {
    render() {
        return (
            <div>
                <div>
                    <h1>EHRENBRUDER EJAZ</h1>
                    <p>Diese Webapp wird freundlicherweise bereitgestellt von friedrich, instandgehalten von @3nt3 und gehostet von dem Ausschuss zur Bereitstellung von sehr guten Webapps der 3nt3rt41nm3nt GbR.</p>
                </div>
                <Randomizer />
            </div>
        )
    }
}

export default Home;